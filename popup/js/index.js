let tabInfo = {};

function getResourceTable(data) {
  let domainInfo = {};

  for(let rType in data.resources) {
    for(let dName in data.resources[ rType ]) {
      if( !domainInfo[ dName ] )
        domainInfo[ dName ] = {};
  
      domainInfo[ dName ][ rType ] = data.resources[ rType ][ dName ].length;
    }
  }

  let table = '<table class="table">';
  table += '<thead><tr>';
  table += '<th title="SRC domain">Domain</th>';
  table += '<th title="Script tags">Scripts</th>';
  table += '<th title="CSS tags">CSS</th>';
  table += '<th title="All media files(images, video etc)">Media</th>';
  table += '<th title="Iframe tags">Iframes</th>';
  table += '</tr></thead><tbody>';

  for(let dName in domainInfo) {
    let scripts = domainInfo[ dName ].scripts || 0;
    let css = domainInfo[ dName ].css || 0;
    let images = domainInfo[ dName ].images || 0;
    let iframes = domainInfo[ dName ].iframes || 0;

    table += '<tr>';
    table += `<td><a href="http://${dName}/" target="_blank">${dName}</a></td>`;
    table += `<td>${scripts}</td>`;
    table += `<td>${css}</td>`;
    table += `<td>${images}</td>`;
    table += `<td>${iframes}</td>`;
    table += '</tr>';
  }

  table += '</tbody></table>';
  return table;
}

function getCookieTable(data) {
  let table = '<table class="table">';
  table += '<thead><tr>';
  table += '<th title="Cookie KEY">Key</th>';
  table += '<th title="Cookie Value">Value</th>';
  table += '<th><a href="javascript: void()" id="clearCookie_button">Clear</></th>';
  table += '</tr></thead><tbody>';

  for(let key in data.cookies) {
    let value = data.cookies[ key ].value;
    table += '<tr>';
    table += `<td><div class="width-limited" title='${key}'>${key}</div></td>`;
    table += `<td><div class="width-limited" title='${value}'>${value}</div></td>`;
    table += `<td><a href="#" rel="${key}" class="cookie_remover">X</a></td>`;
    table += '</tr>';
  }

  table += '</tbody></table>';
  return table;
}

function getStorageTable(data) {
  let table = '<table class="table">';
  table += '<thead><tr>';
  table += '<th title="Storage KEY">Key</th>';
  table += '<th title="Storage Value">Value</th>';
  table += '<th><a href="#" id="clearStorage_button">Clear</></th>';
  table += '</tr></thead><tbody>';

  for(let key in data.storage) {
    let value = JSON.stringify(data.storage[ key ]).replace(/'/g, "`");
    table += '<tr>';
    table += `<td><div class="width-limited" title='${key}'>${key}</div></td>`;
    table += `<td><div class="width-limited" title='${value}'>${value}</div></td>`;
    table += `<td><a href="#" rel="${key}" class="storage_remover">X</a></td>`;
    table += '</tr>';
  }

  table += '</tbody></table>';
  return table;
}

function bindButtons() {

  document.querySelector('#clearCookie_button').addEventListener('click', function(e) {
    e.preventDefault();
    clearCookie();
  }, false);

  document.querySelector('#clearStorage_button').addEventListener('click', function(e) {
    e.preventDefault();
    clearStorage();
  }, false);

}

function render(info) {
  document.querySelector('#website_name').innerHTML = info.domain;
  document.querySelector('#external_resources').innerHTML = getResourceTable(info.data);
  document.querySelector('#cookies_resources').innerHTML = getCookieTable(info.data);
  document.querySelector('#storage_resources').innerHTML = getStorageTable(info.data);
}

async function clearCookie(name=null) {

  if( name == null ) {
    for(let key in tabInfo.data.cookies) {
      await browser.cookies.remove({
        name: key,
        url: tabInfo.protocol + '://' + tabInfo.domain + tabInfo.data.cookies[ key ].path
      });
    }
    // I don't know why, but we need do it twice
    for(let key in tabInfo.data.cookies) {
      await browser.cookies.remove({
        name: key,
        url: tabInfo.protocol + '://' + tabInfo.domain + tabInfo.data.cookies[ key ].path
      });
    }
  } else {
    await browser.cookies.remove({
      name: name,
      url: tabInfo.protocol + '://' + tabInfo.domain + tabInfo.data.cookies[ name ].path
    });
    // I don't know why, but we need do it twice again
    await browser.cookies.remove({
      name: name,
      url: tabInfo.protocol + '://' + tabInfo.domain + tabInfo.data.cookies[ name ].path
    });
  }

  main();
}

async function clearStorage(key=null) {
  if( key == null ) {
    let code = `for(let key in localStorage) if( key != 'length' ) localStorage.removeItem(key);`;
    await browser.tabs.executeScript({
      code: code
    });
  } else {
    let code = `localStorage.removeItem("${key}");`;
    await browser.tabs.executeScript({
      code: code
    });
  }

  main();
}

async function getAllResources() {
  let data = await browser.tabs.executeScript({
    file: 'js/getAllResources.js'
  });

  return data[ 0 ];
}

function switchTab(tabId, link) {
  let tabsHeaders = document.querySelectorAll('.tabs .tabs-header a');
  let tabsTabs = document.querySelectorAll('.tabs .tab');

  for(let index=0; index<tabsHeaders.length; index++)
    tabsHeaders[ index ].classList.remove('active');

  for(let index=0; index<tabsTabs.length; index++)
    tabsTabs[ index ].classList.remove('active');

  link.classList.add('active');
  document.getElementById(tabId).classList.add('active');
}

function bindTabs() {
  let tabsHeaders = document.querySelectorAll('.tabs .tabs-header a');
  for(let index=0; index<tabsHeaders.length; index++) {
    tabsHeaders[ index ].addEventListener('click', function(e) {
      e.preventDefault();
      switchTab(e.target.rel, e.target);
    }, false);
  }

  let cookieButtons = document.querySelectorAll('a.cookie_remover');
  for(let index=0; index<cookieButtons.length; index++) {
    cookieButtons[ index ].addEventListener('click', function(e) {
      e.preventDefault();
      clearCookie(e.target.rel);
    });
  }

  let storageButtons = document.querySelectorAll('a.storage_remover');
  for(let index=0; index<storageButtons.length; index++) {
    storageButtons[ index ].addEventListener('click', function(e) {
      e.preventDefault();
      clearStorage(e.target.rel);
    });
  }
}

async function main() {
  let allTabs = await browser.tabs.query({currentWindow: true, active: true});
  let curTab = allTabs[ 0 ];
  let info = {};

  info.domain = curTab.url.split('/')[ 2 ];
  info.protocol = curTab.url.split(':')[ 0 ];
  info.data = await getAllResources();

  let allCookies = await browser.cookies.getAll({});
  for(let index in allCookies)
    if( (allCookies[ index ].domain == info.domain) || (allCookies[ index ].domain == '.' + info.domain) ) {
      info.data.cookies[ allCookies[ index ].name ] = allCookies[ index ];
    }

  tabInfo = info;
  render(info);
  
  bindTabs();
  bindButtons();
}

main();
