
function parseStorage(storage) {
  let result = {};
  
  for(let key of Object.keys(storage))
    if( key != 'length' )
      result[ key ] = storage[ key ];
  
  return result;
}

function getInfo() {
  let result = {
    resources: {
      css: {},
      images: {},
      scripts: {},
      iframes: {}
    },
    cookies: {},
    storage: parseStorage(localStorage)
  };

  let allIframe = document.getElementsByTagName('iframe');
  for(let index=0; index<allIframe.length; index++) {
    let host = allIframe[ index ].baseURI.split('/')[ 2 ];
    if( allIframe[ index ].src )
      host = allIframe[ index ].src.split('/')[ 2 ];

    if( !host )
      host = 'local';

    if( !result.resources.iframes[ host ] )
      result.resources.iframes[ host ] = [];
    
    result.resources.iframes[ host ].push( allIframe[ index ].src || 'local' );
  }

  let allLinks = document.getElementsByTagName('link');
  for(let index=0; index<allLinks.length; index++)
    if( allLinks[ index ].rel == 'stylesheet' ) {
      let host = allLinks[ index ].baseURI.split('/')[ 2 ];
      if( allLinks[ index ].src )
        host = allLinks[ index ].src.split('/')[ 2 ];

      if( !host )
        host = 'local';

      if( !result.resources.css[ host ] )
        result.resources.css[ host ] = [];
      
      result.resources.css[ host ].push( allLinks[ index ].src || 'local' );
    }

  for(let image of document.images) {
    let host = image.baseURI.split('/')[ 2 ];
    if( image.src )
      host = image.src.split('/')[ 2 ]; 

    if( !host )
      host = 'local';

    if( !result.resources.images[ host ] )
      result.resources.images[ host ] = [];
    
    result.resources.images[ host ].push( image.src || 'local' );
  }

  for(let script of document.scripts) {
    let host = script.baseURI.split('/')[ 2 ];
    if( script.src )
      host = script.src.split('/')[ 2 ]; 

    if( !host )
      host = 'local';

    if( !result.resources.scripts[ host ] )
      result.resources.scripts[ host ] = [];
    
    result.resources.scripts[ host ].push( script.src || 'local' );
  }

  return result;
}

getInfo();

